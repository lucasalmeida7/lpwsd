-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 29-Jun-2019 às 03:22
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lpwsd`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbassunto`
--

CREATE TABLE `tbassunto` (
  `idtbAssunto` int(11) NOT NULL,
  `assunto` varchar(255) DEFAULT NULL,
  `descricaoAssunto` longtext,
  `nomeAssunto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbassunto`
--

INSERT INTO `tbassunto` (`idtbAssunto`, `assunto`, `descricaoAssunto`, `nomeAssunto`) VALUES
(1, 'bca', 'abc', 'abc'),
(2, 'Teste', 'Assunto teste', 'Assunto 123');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbautores`
--

CREATE TABLE `tbautores` (
  `idtbAutores` int(11) NOT NULL,
  `nomeAutor` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbautorlivro`
--

CREATE TABLE `tbautorlivro` (
  `tbLivro_idtbLivro` int(11) NOT NULL,
  `tbAutores_idtbAutores` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbeditora`
--

CREATE TABLE `tbeditora` (
  `idtbEditora` int(11) NOT NULL,
  `nomeEditora` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbemprestimo`
--

CREATE TABLE `tbemprestimo` (
  `idtbEmprestimo` int(11) NOT NULL,
  `dataEmprestimo` datetime DEFAULT NULL,
  `tbExemplar_idtbExemplar` int(11) DEFAULT NULL,
  `tbUsuario_idtbUsuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbexemplar`
--

CREATE TABLE `tbexemplar` (
  `idtbExemplar` int(11) NOT NULL,
  `circular` smallint(6) DEFAULT NULL,
  `tbLivro_idtbLivro` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tblivro`
--

CREATE TABLE `tblivro` (
  `idtbLivro` int(11) NOT NULL,
  `ano` date DEFAULT NULL,
  `edicao` int(11) DEFAULT NULL,
  `isbn` varchar(255) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `tbAssunto_idtbAssunto` int(11) DEFAULT NULL,
  `tbEditora_idtbEditora` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbusuario`
--

CREATE TABLE `tbusuario` (
  `idtbUsuario` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nomeUsuario` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `tipo` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbusuario`
--

INSERT INTO `tbusuario` (`idtbUsuario`, `email`, `nomeUsuario`, `senha`, `tipo`) VALUES
(1, 'magmapt@gmail.com', 'admin', 'admin', 'A'),
(2, 'magmapt@gmail.com', 'lucas', 'lucas', 'B'),
(3, 'magmapt@gmail.com', 'edwward', 'edwward', 'P'),
(4, 'magmapt@gmail.com', 'yato', 'yato', 'L'),
(5, 'magmapt@gmail.com', 'peter', 'peter', 'F');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbassunto`
--
ALTER TABLE `tbassunto`
  ADD PRIMARY KEY (`idtbAssunto`);

--
-- Indexes for table `tbautores`
--
ALTER TABLE `tbautores`
  ADD PRIMARY KEY (`idtbAutores`);

--
-- Indexes for table `tbautorlivro`
--
ALTER TABLE `tbautorlivro`
  ADD PRIMARY KEY (`tbLivro_idtbLivro`,`tbAutores_idtbAutores`),
  ADD KEY `FK_tbAutorLivro_tbAutores_idtbAutores` (`tbAutores_idtbAutores`);

--
-- Indexes for table `tbeditora`
--
ALTER TABLE `tbeditora`
  ADD PRIMARY KEY (`idtbEditora`);

--
-- Indexes for table `tbemprestimo`
--
ALTER TABLE `tbemprestimo`
  ADD PRIMARY KEY (`idtbEmprestimo`),
  ADD KEY `FK_tbEmprestimo_tbExemplar_idtbExemplar` (`tbExemplar_idtbExemplar`),
  ADD KEY `FK_tbEmprestimo_tbUsuario_idtbUsuario` (`tbUsuario_idtbUsuario`);

--
-- Indexes for table `tbexemplar`
--
ALTER TABLE `tbexemplar`
  ADD PRIMARY KEY (`idtbExemplar`),
  ADD KEY `FK_tbExemplar_tbLivro_idtbLivro` (`tbLivro_idtbLivro`);

--
-- Indexes for table `tblivro`
--
ALTER TABLE `tblivro`
  ADD PRIMARY KEY (`idtbLivro`),
  ADD KEY `FK_tbLivro_tbAssunto_idtbAssunto` (`tbAssunto_idtbAssunto`),
  ADD KEY `FK_tbLivro_tbEditora_idtbEditora` (`tbEditora_idtbEditora`);

--
-- Indexes for table `tbusuario`
--
ALTER TABLE `tbusuario`
  ADD PRIMARY KEY (`idtbUsuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbassunto`
--
ALTER TABLE `tbassunto`
  MODIFY `idtbAssunto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbautores`
--
ALTER TABLE `tbautores`
  MODIFY `idtbAutores` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbeditora`
--
ALTER TABLE `tbeditora`
  MODIFY `idtbEditora` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbemprestimo`
--
ALTER TABLE `tbemprestimo`
  MODIFY `idtbEmprestimo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbexemplar`
--
ALTER TABLE `tbexemplar`
  MODIFY `idtbExemplar` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblivro`
--
ALTER TABLE `tblivro`
  MODIFY `idtbLivro` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbusuario`
--
ALTER TABLE `tbusuario`
  MODIFY `idtbUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tbautorlivro`
--
ALTER TABLE `tbautorlivro`
  ADD CONSTRAINT `FK_tbAutorLivro_tbAutores_idtbAutores` FOREIGN KEY (`tbAutores_idtbAutores`) REFERENCES `tbautores` (`idtbAutores`),
  ADD CONSTRAINT `FK_tbAutorLivro_tbLivro_idtbLivro` FOREIGN KEY (`tbLivro_idtbLivro`) REFERENCES `tblivro` (`idtbLivro`);

--
-- Limitadores para a tabela `tbemprestimo`
--
ALTER TABLE `tbemprestimo`
  ADD CONSTRAINT `FK_tbEmprestimo_tbExemplar_idtbExemplar` FOREIGN KEY (`tbExemplar_idtbExemplar`) REFERENCES `tbexemplar` (`idtbExemplar`),
  ADD CONSTRAINT `FK_tbEmprestimo_tbUsuario_idtbUsuario` FOREIGN KEY (`tbUsuario_idtbUsuario`) REFERENCES `tbusuario` (`idtbUsuario`);

--
-- Limitadores para a tabela `tbexemplar`
--
ALTER TABLE `tbexemplar`
  ADD CONSTRAINT `FK_tbExemplar_tbLivro_idtbLivro` FOREIGN KEY (`tbLivro_idtbLivro`) REFERENCES `tblivro` (`idtbLivro`);

--
-- Limitadores para a tabela `tblivro`
--
ALTER TABLE `tblivro`
  ADD CONSTRAINT `FK_tbLivro_tbAssunto_idtbAssunto` FOREIGN KEY (`tbAssunto_idtbAssunto`) REFERENCES `tbassunto` (`idtbAssunto`),
  ADD CONSTRAINT `FK_tbLivro_tbEditora_idtbEditora` FOREIGN KEY (`tbEditora_idtbEditora`) REFERENCES `tbeditora` (`idtbEditora`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
