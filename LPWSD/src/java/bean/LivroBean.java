/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import Modelo.TbLivro;
import dao.LivroDAO;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author alunoces
 */
@ManagedBean
@ViewScoped
public class LivroBean {
    TbLivro livro = new TbLivro();
    
    List livros = new ArrayList();

    public LivroBean() {
        livros = new LivroDAO().buscarTodas();
        livro = new TbLivro();
    }
    
    public void Record(ActionEvent actionEvent)
    {
        new LivroDAO().persistir(livro);
        livros = new LivroDAO().buscarTodas();
        livro = new TbLivro();
    }
    
    public void Exclude(ActionEvent actionEvent)
    {
        new LivroDAO().remover(livro);
        livros = new LivroDAO().buscarTodas();
        livro = new TbLivro();
    }
    
    public TbLivro getLivro()
    {
        return livro;
    }
    
    public void setLivro(TbLivro livro)
    {
        this.livro = livro;
    }
    
    public List getLivros()
    {
        return livros;
    }
    
    public void setLivros(List livros) {
        this.livros = livros;
    }

}
