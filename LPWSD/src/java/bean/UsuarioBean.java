/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import Modelo.TbUsuario;
import dao.UsuarioDAO;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author alunoces
 */
@ManagedBean
@ViewScoped
public class UsuarioBean {
    
    TbUsuario usuario = new TbUsuario();
    
    List usuarios = new ArrayList();

    public UsuarioBean() {
        usuarios = new UsuarioDAO().buscarTodas();
        usuario = new TbUsuario();
    }
    
    public void Record(ActionEvent actionEvent)
    {
        new UsuarioDAO().persistir(usuario);
        usuarios = new UsuarioDAO().buscarTodas();
        usuario = new TbUsuario();
    }
    
    public void Exclude(ActionEvent actionEvent)
    {
        new UsuarioDAO().remover(usuario);
        usuarios = new UsuarioDAO().buscarTodas();
        usuario = new TbUsuario();
    }
    
    public TbUsuario getUsuario()
    {
        return usuario;
    }
    
    public void setUsuario(TbUsuario usuario)
    {
        this.usuario = usuario;
    }
    
    public List getUsuarios()
    {
        return usuarios;
    }
    
    public void setUsuarios(List usuarios) {
        this.usuarios = usuarios;
    }
}
