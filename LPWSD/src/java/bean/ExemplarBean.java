package bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import Modelo.TbExemplar;
import dao.ExemplarDAO;

@ManagedBean
@ViewScoped
public class ExemplarBean {

	TbExemplar exemplar = new TbExemplar();

    List exemplares = new ArrayList();

    public ExemplarBean() {
        exemplares = new ExemplarDAO().buscarTodas();
        exemplar = new TbExemplar();
    }

    public void record(ActionEvent actionEvent) {
        new ExemplarDAO().persistir(exemplar);
        exemplares = new ExemplarDAO().buscarTodas();
        exemplar = new TbExemplar();
    }

    public void exclude(ActionEvent actionEvent) {
        new ExemplarDAO().remover(exemplar);
        exemplares = new ExemplarDAO().buscarTodas();
        exemplar = new TbExemplar();
    }

    //getters and setters
    public TbExemplar getExemplar() {
        return exemplar;
    }

    public void setExemplar(TbExemplar assunto) {
        this.exemplar = exemplar;
    }

    public List getExemplares() {
        return exemplares;
    }

    public void setExemplares(List exemplares) {
        this.exemplares = exemplares;
    }
	
}
