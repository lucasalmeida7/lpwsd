package bean;

import Modelo.TbEditora;

import java.util.ArrayList;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import dao.EditoraDAO;

import java.util.List;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ViewScoped
public class EditoraBean {

    TbEditora editora = new TbEditora();

    List editoras = new ArrayList();

    public EditoraBean() {
        editoras = new EditoraDAO().buscarTodas();
        editora = new TbEditora();
    }

    public void record(ActionEvent actionEvent) {
        new EditoraDAO().persistir(editora);
        editoras = new EditoraDAO().buscarTodas();
        editora = new TbEditora();
    }

    public void exclude(ActionEvent actionEvent) {
        new EditoraDAO().remover(editora);
        editoras = new EditoraDAO().buscarTodas();
        editora = new TbEditora();
    }

    public TbEditora getEditora() {
        return editora;
    }

    public void setEditora(TbEditora editora) {
        this.editora = editora;
    }

    public List getEditoras() {
        return editoras;
    }

    public void setEditoras(List editoras) {
        this.editoras = editoras;
    }

   
}
