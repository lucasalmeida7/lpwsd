/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import Modelo.TbAutores;
import dao.AutoresDAO;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author alunoces
 */
@ManagedBean(name = "autorBean")
@ViewScoped
public class AutorBean {

    TbAutores autor = new TbAutores();
    List autores = new ArrayList();

    public AutorBean() {
        autores = new AutoresDAO().buscarTodas();
        autor = new TbAutores();
    }

    public void record(ActionEvent actionEvent) {
        new AutoresDAO().persistir(autor);
        autores = new AutoresDAO().buscarTodas();
        autor = new TbAutores();
    }

    public void exclude(ActionEvent actionEvent) {
        new AutoresDAO().remover(autor);
        autores = new AutoresDAO().buscarTodas();
        autor = new TbAutores();
    }

    public TbAutores getAutor() {
        return autor;
    }

    public void setAutor(TbAutores autor) {
        this.autor = autor;
    }

    public List getAutores() {
        return autores;
    }

    public void setAutores(List autores) {
        this.autores = autores;
    }

    }
