package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import Modelo.TbExemplar;
import Modelo.TbLivro;
import bd.PersistenceUtil;

public class ExemplarDAO {

public static ExemplarDAO exemplarDAO;
    
    public static ExemplarDAO getInstance() {
        if (exemplarDAO == null) {
            exemplarDAO = new ExemplarDAO();
        }
        return exemplarDAO;
    }
    
    public TbExemplar buscar(String idtbExemplar) {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery("select e from TbExemplar e where e.idtbExemplar =:idtbExemplar ");
        query.setParameter("idtbExemplar", idtbExemplar);

        List<TbExemplar> exemplar = query.getResultList();
        if (exemplar != null && exemplar.size() > 0) {
            return exemplar.get(0);
        }
        
        return null;
    }
    
    public List<TbExemplar> buscarTodas() {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery("from TbExemplar As e");
        return query.getResultList();
    }

    public List<TbExemplar> buscarTbExemplarInstancia() {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery("select distinct e from TbExemplar e");
        return query.getResultList();
    }
    
    public void remover(TbExemplar exemplar) {
        EntityManager em = PersistenceUtil.getEntityManager();
        em.getTransaction().begin();
        if (!em.contains(exemplar)) {
            exemplar = em.merge(exemplar);
        }
        em.remove(exemplar);
        em.getTransaction().commit();
    }
    
    public TbExemplar persistir(TbExemplar exemplar) {
        EntityManager em = PersistenceUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            exemplar = em.merge(exemplar);
            em.getTransaction().commit();
            System.out.println("Registro TbExemplar gravado com sucesso");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return exemplar;
    }
	
	
	
}
