/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Modelo.TbAssunto;
import Modelo.TbLivro;
import bd.PersistenceUtil;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author alunoces
 */
public class LivroDAO implements Serializable {
    
    public static LivroDAO livroDAO;
    
    public static LivroDAO getInstance() {
        if (livroDAO == null) {
            livroDAO = new LivroDAO();
        }
        return livroDAO;
    }
    
    public TbLivro buscar(String titulo) {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery("select a from TbLivro a where a.titulo =:titulo ");
        query.setParameter("titulo", titulo);

        List<TbLivro> livro = query.getResultList();
        if (livro != null && livro.size() > 0) {
            return livro.get(0);
        }
        
        return null;
    }
    
    public List<TbLivro> buscarTodas() {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery("from TbLivro As l");
        return query.getResultList();
    }

    public List<TbLivro> buscarTbLivroInstancia() {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery("select distinct a from TbLivro a group by a.ano");
        return query.getResultList();
    }
    
    public void remover(TbLivro livro) {
        EntityManager em = PersistenceUtil.getEntityManager();
        em.getTransaction().begin();
        if (!em.contains(livro)) {
            livro = em.merge(livro);
        }
        em.remove(livro);
        em.getTransaction().commit();
    }
    
    public TbLivro persistir(TbLivro livro) {
        EntityManager em = PersistenceUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            livro = em.merge(livro);
            em.getTransaction().commit();
            System.out.println("Registro TbLivro gravado com sucesso");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return livro;
    }
    
        public List<TbLivro> buscarPorAssunto(TbAssunto assunto) {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery("from TbLivro As l where tbAssunto_idtbAssunto =:id ");
        query.setParameter("id", assunto.getIdtbAssunto());
        return query.getResultList();
    }
    
}
