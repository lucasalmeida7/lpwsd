/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Modelo.TbAutores;
import bd.PersistenceUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author alunoces
 */
public class AutoresDAO {
 
    public static AutoresDAO autoresDAO;
    
     public static AutoresDAO getInstance() {
        if (autoresDAO == null) {
            autoresDAO = new AutoresDAO();
        }
        return autoresDAO;
    }
    
     public TbAutores buscar(String nome) {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery("select a from TbAutores a where a.nomeAutor =:nome ");
        query.setParameter("nome", nome);

        List<TbAutores> autor = query.getResultList();
        if (autor != null && autor.size() > 0) {
            return autor.get(0);
        }

        return null;
    }
     
    public List<TbAutores> buscarTodas() {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery("from TbAutores As a");
        return query.getResultList();
    }

     public List<TbAutores> buscarTbAutoresInstancia() {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery("select distinct a from TbAutores a");
        return query.getResultList();
    }
    
    public void remover(TbAutores autor) {
        EntityManager em = PersistenceUtil.getEntityManager();
        em.getTransaction().begin();
        if (!em.contains(autor)) {
            autor = em.merge(autor);
        }
        em.remove(autor);
        em.getTransaction().commit();
    }

    public TbAutores persistir(TbAutores autor) {
        EntityManager em = PersistenceUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            autor = em.merge(autor);
            em.getTransaction().commit();
            System.out.println("Registro TbAutores gravado com sucesso");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return autor;
    }

    public void removeAll() {
       EntityManager em = PersistenceUtil.getEntityManager();
       em.getTransaction().begin();
       Query query = em.createQuery(" delete from TbAutores ");
       query.executeUpdate();
       em.getTransaction().commit();
    }
     
}
