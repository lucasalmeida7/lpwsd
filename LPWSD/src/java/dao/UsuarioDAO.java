/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Modelo.TbUsuario;
import bd.PersistenceUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author alunoces
 */
public class UsuarioDAO {
    
    public static UsuarioDAO usuarioDAO;

    public static UsuarioDAO getInstance() {
        if (usuarioDAO == null) {
            usuarioDAO = new UsuarioDAO();
        }
        return usuarioDAO;
    }
    
    public TbUsuario buscar(String nomeUsuario) {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery("select u from TbUsuario u where u.nomeUsuario =:nomeUsuario ");
        query.setParameter("nomeUsuario", nomeUsuario);

        List<TbUsuario> assunto = query.getResultList();
        if (assunto != null && assunto.size() > 0) {
            return assunto.get(0);
        }

        return null;
    }
    
    public TbUsuario buscarUsuario(String nomeUsuario, String senhaUsuario) {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery(
                "select u from TbUsuario u where u.nomeUsuario =:nomeUsuario "
                + "and u.senha = :senhaUsuario");
        query.setParameter("nomeUsuario", nomeUsuario);
        query.setParameter("senhaUsuario", senhaUsuario);

        List<TbUsuario> usuario = query.getResultList();
        if (usuario != null && usuario.size() > 0) {
            return usuario.get(0);
        }

        return null;
    }

    public List<TbUsuario> buscarTodas() {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery("from TbUsuario As u");
        return query.getResultList();
    }

    public List<TbUsuario> buscarTbUsuarioInstancia() {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery("select distinct u from TbUsuario u ");
        return query.getResultList();
    }
    
    public void remover(TbUsuario usuario) {
        EntityManager em = PersistenceUtil.getEntityManager();
        em.getTransaction().begin();
        if (!em.contains(usuario)) {
            usuario = em.merge(usuario);
        }
        em.remove(usuario);
        em.getTransaction().commit();
    }

    public TbUsuario persistir(TbUsuario usuario) {
        EntityManager em = PersistenceUtil.getEntityManager();
        em.getTransaction().begin();
        try {
            usuario = em.merge(usuario);
            em.getTransaction().commit();
            System.out.println("Registro TbUsuario gravado com sucesso");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return usuario;
    }

    public void removeAll() {
       EntityManager em = PersistenceUtil.getEntityManager();
       em.getTransaction().begin();
       Query query = em.createQuery(" delete from TbUsuario ");
       query.executeUpdate();
       em.getTransaction().commit();
    }

    public TbUsuario getLogin(TbUsuario usuario) {
        EntityManager em = PersistenceUtil.getEntityManager();
        Query query = em.createQuery(
            "select u from TbUsuario u where u.nomeUsuario =:nome "
            + "and u.senha = :senha");
        query.setParameter("nome", usuario.getNomeUsuario());
        query.setParameter("senha", usuario.getSenha());
        
        List<TbUsuario> usuariosRetornados = query.getResultList();
        if (usuariosRetornados != null && usuariosRetornados.size() > 0) {
            return usuariosRetornados.get(0);
        }

        return null;
    }

}
