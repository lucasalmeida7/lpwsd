/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chart;

import Modelo.TbAssunto;
import Modelo.TbLivro;
import dao.AssuntoDAO;
import dao.LivroDAO;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

/**
 *
 * @author Lucas
 */
@ManagedBean
@RequestScoped
public class AssuntoChart implements Serializable {

    private CartesianChartModel model;
    
    public void criaModeloBarras() {
        model = new CartesianChartModel();

        AssuntoDAO assuntoDAO = new AssuntoDAO();
        List<TbAssunto>  assuntos = new AssuntoDAO().buscarTodas();

        ChartSeries assuntoGrafico;

        for (int i = 0; i < assuntos.size(); i++) {
            assuntoGrafico = new ChartSeries();
            //Hibernate retorna os valores num array onde na
            //posição 0 temos o nome da cidade e na posição 1
            //temos a quantidade de incidentes.
            TbAssunto assunto = assuntos.get(i);
            String label = String.valueOf(assunto.getNomeAssunto());
            
            List<TbLivro> livros = new LivroDAO().buscarPorAssunto(assunto);
            String qtdLivros = String.valueOf(livros.size());

            if (Integer.valueOf(qtdLivros) > 0) {
                assuntoGrafico.setLabel(label);
                //Apenas do mês estar fixo podemos alterar para
                //receber valor de um combo.
                assuntoGrafico.set("Quantidade", Integer.
                        valueOf(qtdLivros));
                //Adiciona a cidade com seu valor ao gráfico
                model.addSeries(assuntoGrafico);
            }

            assuntoGrafico = null;
        }

        FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().put("model", this.model);
    }

    public void setModel(CartesianChartModel model) {
        this.model = model;
    }

    public CartesianChartModel getModel() {
        //Retorna o modelo adicionado anteriormente na sessão
        CartesianChartModel model = (CartesianChartModel) FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().get("model");

        if (model != null) {
            return model;
        }

        return new CartesianChartModel();
    }

}
